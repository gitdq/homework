#include "stdio.h"
#include "stdlib.h"
#include "string.h"

typedef struct Node
{
  bool isC,isW,isL,isO;//记录传入的参数
  char fileName[50];//要统计的文件名字
  char outFile[50];//要输出的文件名字
  int charNum,wordNum,lineNum;//统计结果
}Node;
Node node;

void charNum() //字符统计函数
{
    FILE *fp,*fp2;
    char ch;
    if((fp = fopen(node.fileName,"r")) == NULL)
    {
        printf("file read failure.");
        exit(0); 
    }
    ch = fgetc(fp);
    while(ch != EOF)
    {
            node.charNum++;
            ch = fgetc(fp);
    }
    if((fp2 = fopen(node.outFile,"a+")) == NULL)
    {
        printf("file read failure.");
        exit(0); 
    }
	printf("%s,字符数:%d\n",node.fileName,node.charNum);
    fprintf(fp2,"%s,字符数:%d\n",node.fileName,node.charNum);   
    fclose(fp2);
    fclose(fp);
}

void wordNum() //单词统计函数
{
    FILE *fp,*fp2;
    char ch;
    if((fp = fopen(node.fileName,"r")) == NULL)
    {
        printf("file read failure.");
        exit(0); 
    }
    ch = fgetc(fp);
    while(ch != EOF)
    {
        if ((ch >= 'a'&&ch <= 'z')||(ch >= 'A'&&ch <='Z')||(ch >= '0'&&ch <= '9'))
        {
            while ((ch >= 'a'&&ch <= 'z')||(ch >= 'A'&&ch <= 'Z')||(ch >= '0'&&ch <= '9')||ch == '_')
            {
                ch = fgetc(fp);
            }
            node.wordNum++;
            ch = fgetc(fp);    
        }
        else 
        {
            ch = fgetc(fp);
        }
    }
    if((fp2 = fopen(node.outFile,"a+")) == NULL)
    {
        printf("file read failure.");
        exit(0); 
    }
	printf("%s,单词数:%d\n",node.fileName,node.wordNum);
    fprintf(fp2,"%s,单词数:%d\n",node.fileName,node.wordNum);   
    fclose(fp2);
    fclose(fp);

}

void lineNum() //行数统计函数
{
    FILE *fp,*fp2;
    char ch;
    if((fp = fopen(node.fileName,"r")) == NULL)
    {
        printf("file read failure.");
        exit(0); 
    }
    ch = fgetc(fp);
    while(ch != EOF)
    {
        if (ch == '\n')
        {
            node.lineNum++;
            ch = fgetc(fp);
        }
        else
        {
            ch = fgetc(fp);
        }
    }
    if((fp2 = fopen(node.outFile,"a+")) == NULL)
    {
        printf("file read failure.");
        exit(0); 
    }
	printf("%s,行数:%d\n",node.fileName,node.lineNum);
    fprintf(fp2,"%s,行数:%d\n",node.fileName,node.lineNum);   
    fclose(fp2);
    fclose(fp);
}

void initNode(int num,char* str[])
{
	node.isC=false;
  	node.isW=false;
 	node.isL=false;
  	node.isO=false;
  	strcpy(node.outFile,"result.txt");
  	node.charNum=0;
  	node.wordNum=0;
  	node.lineNum=1;
  	if(strcmp(str[1],"-o")==0)
  	{
  		printf("命令参数出错！");
	   	exit(0);
  	}
	for(int i=1;i<num;i++)
	{
		if(str[i][0]=='-')
		{
			if(str[i][1]=='c')
       			node.isC=true;
       		if(str[i][1]=='w')
       			node.isW=true;
       		if(str[i][1]=='l')
       			node.isL=true;
       		if(str[i][1]=='o')
       		{
       			if(str[i+1][0]!='-')
	   			{
	   				strcpy(node.outFile,str[i+1]);
	   				i++;
	   			}
	   			else
	   			{
	   				printf("命令参数出错！");
	   				exit(0);
	   			}
	   			node.isO=true;
       		}
   		}
   		else
   		{
   			if(strcmp(str[i-1],"-c")==0||strcmp(str[i-1],"-w")==0||strcmp(str[i-1],"-l")==0)
			{
				strcpy(node.fileName,str[i]);
			}
			else
   			{
   				printf("命令参数出错！");
   				exit(0);
   			}
   		}
	}	
}

void analyse()
{
	if(node.isC)
	{
		charNum();
	}
	if(node.isW)
	{
		wordNum();
	}
	if(node.isL)
	{
		lineNum();
	}
}

int main(int argc,char *argv[])
{
	initNode(argc,argv);
    analyse();   
    return 0;
}
